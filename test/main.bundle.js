webpackJsonp(["main"],{

/***/ "../../../../../src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "../../../../../src/app/app-routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_status_status_component__ = __webpack_require__("../../../../../src/app/pages/status/status.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_register_register_component__ = __webpack_require__("../../../../../src/app/pages/register/register.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var routes = [
    { path: '', redirectTo: '/register', pathMatch: 'full' },
    { path: 'register', component: __WEBPACK_IMPORTED_MODULE_3__pages_register_register_component__["a" /* RegisterComponent */] },
    { path: 'status', component: __WEBPACK_IMPORTED_MODULE_2__pages_status_status_component__["a" /* StatusComponent */] }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* RouterModule */].forRoot(routes)],
            exports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* RouterModule */]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "../../../../../src/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "\n<router-outlet></router-outlet>\n"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
    }
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-root',
            template: __webpack_require__("../../../../../src/app/app.component.html"),
            styles: [__webpack_require__("../../../../../src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_routing_module__ = __webpack_require__("../../../../../src/app/app-routing.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser_animations__ = __webpack_require__("../../../platform-browser/esm5/animations.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__material_module__ = __webpack_require__("../../../../../src/app/material.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ng_recaptcha__ = __webpack_require__("../../../../ng-recaptcha/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ng_recaptcha___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_ng_recaptcha__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_angular2_qrcode__ = __webpack_require__("../../../../angular2-qrcode/lib/angular2-qrcode.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_status_status_component__ = __webpack_require__("../../../../../src/app/pages/status/status.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_register_register_component__ = __webpack_require__("../../../../../src/app/pages/register/register.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};











var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_9__pages_status_status_component__["a" /* StatusComponent */],
                __WEBPACK_IMPORTED_MODULE_10__pages_register_register_component__["a" /* RegisterComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */], __WEBPACK_IMPORTED_MODULE_5__material_module__["a" /* MaterialModule */], __WEBPACK_IMPORTED_MODULE_6_ng_recaptcha__["RecaptchaModule"].forRoot(),
                __WEBPACK_IMPORTED_MODULE_2__app_routing_module__["a" /* AppRoutingModule */], __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */],
                __WEBPACK_IMPORTED_MODULE_7__angular_http__["b" /* HttpModule */], __WEBPACK_IMPORTED_MODULE_8_angular2_qrcode__["a" /* QRCodeModule */]
            ],
            providers: [],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "../../../../../src/app/material.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MaterialModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_material_icon__ = __webpack_require__("../../../material/esm5/icon.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_material_card__ = __webpack_require__("../../../material/esm5/card.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_material_stepper__ = __webpack_require__("../../../material/esm5/stepper.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_material_form_field__ = __webpack_require__("../../../material/esm5/form-field.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_material_select__ = __webpack_require__("../../../material/esm5/select.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_material_checkbox__ = __webpack_require__("../../../material/esm5/checkbox.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__angular_material_divider__ = __webpack_require__("../../../material/esm5/divider.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__angular_material_list__ = __webpack_require__("../../../material/esm5/list.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__angular_material_progress_spinner__ = __webpack_require__("../../../material/esm5/progress-spinner.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};













var MaterialModule = /** @class */ (function () {
    function MaterialModule() {
    }
    MaterialModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [__WEBPACK_IMPORTED_MODULE_1__angular_material__["a" /* MatButtonModule */], __WEBPACK_IMPORTED_MODULE_1__angular_material__["c" /* MatToolbarModule */], __WEBPACK_IMPORTED_MODULE_2__angular_material_icon__["a" /* MatIconModule */], __WEBPACK_IMPORTED_MODULE_3__angular_material_card__["a" /* MatCardModule */], __WEBPACK_IMPORTED_MODULE_4__angular_material_stepper__["a" /* MatStepperModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_material_form_field__["c" /* MatFormFieldModule */], __WEBPACK_IMPORTED_MODULE_8__angular_forms__["d" /* FormsModule */], __WEBPACK_IMPORTED_MODULE_8__angular_forms__["i" /* ReactiveFormsModule */], __WEBPACK_IMPORTED_MODULE_1__angular_material__["b" /* MatInputModule */], __WEBPACK_IMPORTED_MODULE_6__angular_material_select__["a" /* MatSelectModule */],
                __WEBPACK_IMPORTED_MODULE_7__angular_material_checkbox__["a" /* MatCheckboxModule */], __WEBPACK_IMPORTED_MODULE_9__angular_material_divider__["a" /* MatDividerModule */], __WEBPACK_IMPORTED_MODULE_10__angular_material_list__["a" /* MatListModule */], __WEBPACK_IMPORTED_MODULE_11__angular_material_progress_spinner__["a" /* MatProgressSpinnerModule */]
            ],
            exports: [__WEBPACK_IMPORTED_MODULE_1__angular_material__["a" /* MatButtonModule */], __WEBPACK_IMPORTED_MODULE_1__angular_material__["c" /* MatToolbarModule */], __WEBPACK_IMPORTED_MODULE_2__angular_material_icon__["a" /* MatIconModule */], __WEBPACK_IMPORTED_MODULE_3__angular_material_card__["a" /* MatCardModule */], __WEBPACK_IMPORTED_MODULE_4__angular_material_stepper__["a" /* MatStepperModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_material_form_field__["c" /* MatFormFieldModule */], __WEBPACK_IMPORTED_MODULE_8__angular_forms__["d" /* FormsModule */], __WEBPACK_IMPORTED_MODULE_8__angular_forms__["i" /* ReactiveFormsModule */], __WEBPACK_IMPORTED_MODULE_1__angular_material__["b" /* MatInputModule */], __WEBPACK_IMPORTED_MODULE_6__angular_material_select__["a" /* MatSelectModule */],
                __WEBPACK_IMPORTED_MODULE_7__angular_material_checkbox__["a" /* MatCheckboxModule */], __WEBPACK_IMPORTED_MODULE_9__angular_material_divider__["a" /* MatDividerModule */], __WEBPACK_IMPORTED_MODULE_10__angular_material_list__["a" /* MatListModule */], __WEBPACK_IMPORTED_MODULE_11__angular_material_progress_spinner__["a" /* MatProgressSpinnerModule */]
            ],
        })
    ], MaterialModule);
    return MaterialModule;
}());



/***/ }),

/***/ "../../../../../src/app/pages/register/register.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".example-icon {\n  padding: 0 14px;\n}\n\n.example-spacer {\n  -webkit-box-flex: 1;\n      -ms-flex: 1 1 auto;\n          flex: 1 1 auto;\n}\n\n.example-card {\n  width: 520px;margin-left: auto;margin-right: auto;\n  margin-top: 10px;background: none !important;\n}\n\n.example-header-image {\n  background-size: cover;\n}\n\nexample-form {\n  min-width: 150px;\n  max-width: 480px;\n  width: 100%;\n}\n\n.example-full-width {\n  width: 100%;\n}\n\n.s-width {\n  width: 154px;\n}\n\n.mat-button, .mat-fab, .mat-icon-button, .mat-mini-fab, .mat-raised-button\n{\n\tcolor: black;\n}\n\n.minus{\n      color: #fff;\n    background-color: #d9534f;\n    border-color: #d43f3a;\n    padding: 0;\n}\n\n.add\n{\n      color: #fff;\n    background-color: #5cb85c;\n    border-color: #4cae4c;\n        padding: 0;\n}\n\n.wid{\n  width: 50px !important;\n}\n\n.fill-space {\n  -webkit-box-flex: 1;\n      -ms-flex: 1 1 auto;\n          flex: 1 1 auto;\n}\n\n.mat-horizontal-stepper-header{\n    pointer-events: none !important;\n}\n\n.mat-card{\n  padding: 10px;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/pages/register/register.component.html":
/***/ (function(module, exports) {

module.exports = "<mat-card class=\"example-card\">\n  <mat-card-header>\n    <mat-card-title style=\"color:white\">\n      <b>REGISTRATION FORM</b>\n    </mat-card-title>\n  </mat-card-header>\n  <mat-card-content>\n\n    <mat-horizontal-stepper #stepper>\n      <mat-step>\n        <form [formGroup]=\"firstFormGroup\">\n          <ng-template matStepLabel>Fill out the form</ng-template>\n          <div>\n            <mat-form-field class=\"s-width\">\n              <input matInput placeholder=\"Last name\" formControlName=\"lastCtrl\" required>\n              <mat-hint>Reference to the Cerificate.</mat-hint>\n            </mat-form-field>\n            <mat-form-field class=\"s-width\">\n              <input matInput placeholder=\"First name\" formControlName=\"firstCtrl\" required>\n            </mat-form-field>\n            <mat-form-field class=\"s-width\">\n              <input matInput placeholder=\"Middle name\" formControlName=\"middleCtrl\">\n            </mat-form-field>\n            <mat-form-field class=\"example-full-width\">\n              <input matInput placeholder=\"Email address\" formControlName=\"emailCtrl\" required=\"\">\n              <mat-hint>E-receipt/E-Certificate will be sent to your email</mat-hint>\n            </mat-form-field>\n            <mat-form-field class=\"example-full-width\">\n              <input matInput placeholder=\"Mobile number\" formControlName=\"mobileCtrl\">\n            </mat-form-field>\n            <mat-form-field>\n              <mat-select placeholder=\"Category\" formControlName=\"occuCtrl\" required (change)=\"onChange()\">\n                <mat-option value=\"Student\">Student</mat-option>\n                <mat-option value=\"Professional\">Professional</mat-option>\n              </mat-select>\n            </mat-form-field>\n            <mat-form-field class=\"example-full-width\">\n              <input matInput placeholder=\"Agency/Company/University\" formControlName=\"asCtrl\" required>\n            </mat-form-field>\n          </div>\n          <div formArrayName=\"additionalParticipants\" *ngFor=\"let p of firstFormGroup.get('additionalParticipants').controls; let i = index;\">\n            <div [formGroupName]=\"i\">\n              <mat-form-field class=\"s-width\">\n                <input matInput placeholder=\"Last name\" formControlName=\"last_name\" required>\n                <mat-hint>Reference to the Cerificate.</mat-hint>\n              </mat-form-field>\n              <mat-form-field class=\"s-width\">\n                <input matInput placeholder=\"First name\" formControlName=\"first_name\" required>\n              </mat-form-field>\n              <mat-form-field class=\"s-width\">\n                <input matInput placeholder=\"Middle name\" formControlName=\"middle_name\">\n              </mat-form-field>\n              <mat-form-field class=\"example-full-width\">\n                <input matInput placeholder=\"Email address\" formControlName=\"email\" required=\"\">\n                <mat-hint>E-receipt/E-Certificate will be sent to your email</mat-hint>\n              </mat-form-field>\n              <mat-form-field>\n                <mat-select placeholder=\"Category\" formControlName=\"occupation\" required>\n                  <mat-option value=\"Student\">Student</mat-option>\n                  <mat-option value=\"Professional\">Professional</mat-option>\n                </mat-select>\n              </mat-form-field>\n              <mat-form-field class=\"example-full-width\">\n                <input matInput placeholder=\"Agency/Company/University\" formControlName=\"agency\" required>\n              </mat-form-field>\n              <button type=\"button\" (click)=\"removeParticipant(i)\"  mat-raised-button color=\"warn\" style=\"color: white\">\n                <i class=\"material-icons\">\n                  remove_circle_outline\n                  </i> Remove\n                </button>\n\n            </div>\n          </div>\n          <br>\n          <button type=\"button\" (click)=\"addParticipant()\" mat-raised-button color=\"primary\" style=\"color: white\"><i class=\"material-icons\">\n          group_add\n          </i> Add Participants</button>\n\n          <br><br>\n            <mat-form-field class=\"example-full-width\">\n              <input matInput placeholder=\"Code reference\" formControlName=\"codeCtrl\">\n              <mat-hint>Code given by a goon of HTN (leave blank if none).</mat-hint>\n            </mat-form-field>\n          <div>\n            <br>\n            <button mat-button type=\"submit\" style=\"color: white;background-color: black\" (click)=\"add(stepper)\">Next</button>\n          </div>\n        </form>\n      </mat-step>\n      <mat-step>\n        <form>\n          <ng-template matStepLabel>Reservation Details</ng-template>\n          <p>Select the dates you want to attend.</p>\n          <div>\n            <mat-card style=\"background-color: #f4f4f4;\">\n              <mat-checkbox (change)=\"click1()\" [checked]=\"isChecked\">Day 1 (IT Security)\n                <br>\n                <small>November 30, 2018</small>\n              </mat-checkbox>\n            </mat-card>\n            <mat-card style=\"background-color: #f4f4f4;margin-top: 5px\">\n              <mat-checkbox (change)=\"click2()\" [checked]=\"isChecked2\">Day 2 (Debug)\n                <br>\n                <small>December 1, 2018 </small>\n              </mat-checkbox>\n            </mat-card>\n            <mat-card style=\"background-color: #f4f4f4;margin-top: 5px\">\n              <mat-checkbox (change)=\"click3()\" [checked]=\"isChecked3\">Day 3 (Hackers on the Beach)\n                <br>\n                <small>December 2, 2018</small>\n              </mat-checkbox>\n            </mat-card>\n\n          </div>\n          <div *ngIf=\"day1!=0\">\n          <br>\n          <p>Day 1:</p>\n          <mat-list>\n            <mat-list-item>Professionals (&#8369;1250)\n              <span class=\"fill-space\"></span>\n              <div style=\"text-align: right !important;\">\n                * {{noofprof}}\n              </div>\n            </mat-list-item>\n\n            <mat-list-item>Students (&#8369;1000)\n              <span class=\"fill-space\"></span>\n              <div style=\"text-align: right !important;\">\n                * {{noofstud}}\n              </div>\n            </mat-list-item>\n          </mat-list>\n          </div>\n\n\n          <div *ngIf=\"day2!=0\">\n          <br>\n          <p>Day 2:</p>\n          <mat-list>\n            <mat-list-item>Professionals (&#8369;1250)\n              <span class=\"fill-space\"></span>\n              <div style=\"text-align: right !important;\">\n                * {{noofprof}}\n              </div>\n            </mat-list-item>\n\n            <mat-list-item>Students (&#8369;1000)\n              <span class=\"fill-space\"></span>\n              <div style=\"text-align: right !important;\">\n                * {{noofstud}}\n              </div>\n            </mat-list-item>\n          </mat-list>\n          </div>\n\n\n          <div *ngIf=\"day3!=0\">\n          <br>\n          <p>Day 3:</p>\n          <mat-list>\n            <mat-list-item>Professionals (&#8369;1500)\n              <span class=\"fill-space\"></span>\n              <div style=\"text-align: right !important;\">\n                * {{noofprof}}\n              </div>\n            </mat-list-item>\n\n            <mat-list-item>Students (&#8369;1500)\n              <span class=\"fill-space\"></span>\n              <div style=\"text-align: right !important;\">\n                 * {{noofstud}}\n              </div>\n            </mat-list-item>\n          </mat-list>\n          </div>\n\n\n          <br>\n          <p>Pre-order your shirt here!</p>\n          <mat-list>\n            <mat-list-item>Small (&#8369;300)\n              <span class=\"fill-space\"></span>\n              <div style=\"text-align: right !important;\">\n                <button mat-button class=\"minus\" (click)=\"smallminus()\">\n                  <mat-icon>remove</mat-icon>\n                </button>\n                <mat-form-field class=\"wid\">\n                  <input matInput value=\"{{small}}\" type=\"text\" readonly>\n                </mat-form-field>\n                <button mat-button class=\"add\" (click)=\"smalladd()\">\n                  <mat-icon>add</mat-icon>\n                </button>\n              </div>\n            </mat-list-item>\n            <mat-list-item>Medium (&#8369;300)\n              <span class=\"fill-space\"></span>\n              <div style=\"text-align: right !important;\">\n                <button mat-button class=\"minus\" (click)=\"mminus()\">\n                  <mat-icon>remove</mat-icon>\n                </button>\n                <mat-form-field class=\"wid\">\n                  <input matInput value=\"{{medium}}\" type=\"text\" readonly>\n                </mat-form-field>\n                <button mat-button class=\"add\" (click)=\"madd()\">\n                  <mat-icon>add</mat-icon>\n                </button>\n              </div>\n            </mat-list-item>\n            <mat-list-item>Large (&#8369;350)\n              <span class=\"fill-space\"></span>\n              <div style=\"text-align: right !important;\">\n                <button mat-button class=\"minus\" (click)=\"lminus()\">\n                  <mat-icon>remove</mat-icon>\n                </button>\n                <mat-form-field class=\"wid\">\n                  <input matInput value=\"{{large}}\" type=\"text\" readonly>\n                </mat-form-field>\n                <button mat-button class=\"add\" (click)=\"ladd()\">\n                  <mat-icon>add</mat-icon>\n                </button>\n              </div>\n            </mat-list-item>\n            <mat-list-item>X Large (&#8369;350)\n              <span class=\"fill-space\"></span>\n              <div style=\"text-align: right !important;\">\n                <button mat-button class=\"minus\" (click)=\"xlminus()\">\n                  <mat-icon>remove</mat-icon>\n                </button>\n                <mat-form-field class=\"wid\">\n                  <input matInput value=\"{{xlarge}}\" type=\"text\" readonly>\n                </mat-form-field>\n                <button mat-button class=\"add\" (click)=\"xladd()\">\n                  <mat-icon>add</mat-icon>\n                </button>\n              </div>\n            </mat-list-item>\n\n            <mat-list-item>XX Large (&#8369;350)\n              <span class=\"fill-space\"></span>\n              <div style=\"text-align: right !important;\">\n                <button mat-button class=\"minus\" (click)=\"xxlminus()\">\n                  <mat-icon>remove</mat-icon>\n                </button>\n                <mat-form-field class=\"wid\">\n                  <input matInput value=\"{{xxlarge}}\" type=\"text\" readonly>\n                </mat-form-field>\n                <button mat-button class=\"add\" (click)=\"xxladd()\">\n                  <mat-icon>add</mat-icon>\n                </button>\n              </div>\n            </mat-list-item>\n\n            <mat-list-item>XXX Large (&#8369;350)\n              <span class=\"fill-space\"></span>\n              <div style=\"text-align: right !important;\">\n                <button mat-button class=\"minus\" (click)=\"xxxlminus()\">\n                  <mat-icon>remove</mat-icon>\n                </button>\n                <mat-form-field class=\"wid\">\n                  <input matInput value=\"{{xxxlarge}}\" type=\"text\" readonly>\n                </mat-form-field>\n                <button mat-button class=\"add\" (click)=\"xxxladd()\">\n                  <mat-icon>add</mat-icon>\n                </button>\n              </div>\n            </mat-list-item>\n          </mat-list>\n          <h2 style=\"text-align: right;margin-right: 13px\">Total Price: &#8369;{{total}}</h2>\n          <div>\n            <re-captcha (resolved)=\"resolved($event)\" siteKey=\"6LcYNhkUAAAAAEsanGHbtsgLL7bhF_LoHnGxmHyT\" *ngIf=\"rcode!='final'\"></re-captcha>\n            <p *ngIf=\"rcode==='final'\">You are registered!</p>\n            <br>\n            <button mat-button matStepperPrevious style=\"color: white;background-color: black\" (click)=\"resetCart()\">Back</button>\n            <mat-spinner *ngIf=\"rcode==='load'\" style=\"float: right;\"  ></mat-spinner>\n            <button mat-button style=\"color: white;background-color: black;float: right;\" (click)=\"register(stepper)\" *ngIf=\"rcode!='load'\">Register</button>\n            <button mat-button matStepperNext style=\"color: white;background-color: black;float: right;\" *ngIf=\"rcode==='final'\">Next</button>\n          </div>\n        </form>\n      </mat-step>\n      <mat-step>\n        <ng-template matStepLabel>Summary</ng-template>\n\n        <div>\n\n          <div *ngIf=\"day1!=0\">\n          <p>Day 1:</p>\n          <mat-list>\n            <mat-list-item>Professionals (&#8369;1250) * {{noofprof}}\n              <span class=\"fill-space\"></span>\n              <div style=\"text-align: right !important;\">\n                &#8369;{{ 1250*noofprof}}\n              </div>\n            </mat-list-item>\n\n            <mat-list-item>Students (&#8369;1000) * {{noofstud}}\n              <span class=\"fill-space\"></span>\n              <div style=\"text-align: right !important;\">\n                 &#8369;{{ 1000*noofstud}}\n              </div>\n            </mat-list-item>\n          </mat-list>\n          </div>\n        \n        <div *ngIf=\"day2!=0\">\n          <p>Day 2:</p>\n          <mat-list>\n            <mat-list-item>Professionals (&#8369;1250) * {{noofprof}}\n              <span class=\"fill-space\"></span>\n              <div style=\"text-align: right !important;\">\n                &#8369;{{ 1250*noofprof}}\n              </div>\n            </mat-list-item>\n\n            <mat-list-item>Students (&#8369;1000) * {{noofstud}}\n              <span class=\"fill-space\"></span>\n              <div style=\"text-align: right !important;\">\n                 &#8369;{{ 1000*noofstud}}\n              </div>\n            </mat-list-item>\n          </mat-list>\n          </div>\n        </div>\n\n        <div *ngIf=\"day3!=0\">\n          <p>Day 3:</p>\n          <mat-list>\n            <mat-list-item>Professionals (&#8369;1500) * {{noofprof}}\n              <span class=\"fill-space\"></span>\n              <div style=\"text-align: right !important;\">\n                &#8369;{{ 1500*noofprof}}\n              </div>\n            </mat-list-item>\n\n            <mat-list-item>Students (&#8369;1500) * {{noofstud}}\n              <span class=\"fill-space\"></span>\n              <div style=\"text-align: right !important;\">\n                 &#8369;{{ 1500*noofstud}}\n              </div>\n            </mat-list-item>\n          </mat-list>\n          </div>\n        <mat-list>\n          <mat-list-item *ngIf=\"small>0\">Small * {{small}}\n            <span class=\"fill-space\"></span>\n            <div style=\"text-align: right !important;\">\n              <p>&#8369;{{300 * small}}</p>\n            </div>\n          </mat-list-item>\n\n          <mat-list-item *ngIf=\"medium>0\">Medium * {{medium}}\n            <span class=\"fill-space\"></span>\n            <div style=\"text-align: right !important;\">\n              <p>&#8369;{{300 * medium}}</p>\n            </div>\n          </mat-list-item>\n\n          <mat-list-item *ngIf=\"large>0\">Large * {{large}}\n            <span class=\"fill-space\"></span>\n            <div style=\"text-align: right !important;\">\n              <p>&#8369;{{350 * large}}</p>\n            </div>\n          </mat-list-item>\n\n          <mat-list-item *ngIf=\"xlarge>0\">X Large * {{xlarge}}\n            <span class=\"fill-space\"></span>\n            <div style=\"text-align: right !important;\">\n              <p>&#8369;{{350 * xlarge}}</p>\n            </div>\n          </mat-list-item>\n          <mat-list-item *ngIf=\"xxlarge>0\">XX Large * {{xlarge}}\n            <span class=\"fill-space\"></span>\n            <div style=\"text-align: right !important;\">\n              <p>&#8369;{{350 * xxlarge}}</p>\n            </div>\n          </mat-list-item>\n          <mat-list-item *ngIf=\"xxxlarge>0\">XXX Large * {{xlarge}}\n            <span class=\"fill-space\"></span>\n            <div style=\"text-align: right !important;\">\n              <p>&#8369;{{350 * xxxlarge}}</p>\n            </div>\n          </mat-list-item>\n          <mat-list-item *ngIf=\"total>0\">\n            <span class=\"fill-space\"></span>\n            <h3>\n              <b>Total - &#8369;{{total}}</b>\n            </h3>\n          </mat-list-item>\n        </mat-list>\n\n\n        <h3 *ngIf=\"rcode==='none'\">\n          <b>{{code}}</b>\n        </h3>\n        <h1 *ngIf=\"rcode==='final'\">\n          <b>Your Code: {{code}}</b>\n        </h1>\n        <h3 *ngIf=\"rcode==='fail'\">\n          <b>Failed!</b>\n        </h3>\n        <mat-spinner *ngIf=\"rcode==='load'\"></mat-spinner>\n        <p>{{code2}}</p>\n        <br>\n        <br>\n        <div>\n          <a href=\".\">\n            <button mat-button style=\"color: white;background-color: black;float: right;\">Done</button>\n          </a>\n        </div>\n      </mat-step>\n    </mat-horizontal-stepper>\n\n  </mat-card-content>\n</mat-card>"

/***/ }),

/***/ "../../../../../src/app/pages/register/register.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_toPromise__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/toPromise.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_toPromise___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_toPromise__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var RegisterComponent = /** @class */ (function () {
    function RegisterComponent(_formBuilder, http) {
        this._formBuilder = _formBuilder;
        this.http = http;
        this.noofprof = 0;
        this.noofstud = 0;
        this.isChecked = false;
        this.isChecked3 = false;
        this.isChecked2 = false;
        this.rcode = 'none';
        this.hide = "none";
        this.isLinear = false;
        this.total = 0;
        this.day1 = 0;
        this.day2 = 0;
        this.day3 = 0;
        this.small = 0;
        this.medium = 0;
        this.large = 0;
        this.xlarge = 0;
        this.xxlarge = 0;
        this.xxxlarge = 0;
        this.code = "Register First to get your Unique Code!";
        this.code2 = "The code will be used for your attendance, else you will be obliged to encode your name in the booth during the seminar.";
        this.additionalParticipants = [];
    }
    RegisterComponent.prototype.resolved = function (captchaResponse) {
        if (captchaResponse != null || captchaResponse != undefined || captchaResponse != '') {
            this.checked = 1;
        }
        else {
            this.checked = 0;
        }
    };
    RegisterComponent.prototype.ngOnInit = function () {
        this.firstFormGroup = this._formBuilder.group({
            firstCtrl: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["j" /* Validators */].required],
            middleCtrl: [''],
            lastCtrl: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["j" /* Validators */].required],
            emailCtrl: [''],
            mobileCtrl: [''],
            codeCtrl: [''],
            asCtrl: [''],
            occuCtrl: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["j" /* Validators */].required],
            additionalParticipants: this._formBuilder.array([this.createParticipant()])
        });
        this.additionalParticipants = this.firstFormGroup.get('additionalParticipants');
        this.additionalParticipants.removeAt(0);
    };
    RegisterComponent.prototype.createParticipant = function () {
        return this._formBuilder.group({
            first_name: '',
            middle_name: '',
            last_name: '',
            occupation: '',
            email: '',
            agency: ''
        });
    };
    RegisterComponent.prototype.addParticipant = function () {
        this.additionalParticipants = this.firstFormGroup.get('additionalParticipants');
        this.additionalParticipants.push(this.createParticipant());
        console.log("Additional Participants", this.additionalParticipants.value.length);
    };
    RegisterComponent.prototype.removeParticipant = function (index) {
        if (confirm("Are you sure you want to remove this participant?")) {
            console.log("Removing2", index);
            this.additionalParticipants = this.firstFormGroup.get('additionalParticipants');
            this.additionalParticipants.removeAt(index);
        }
    };
    RegisterComponent.prototype.smalladd = function () {
        this.small += 1;
        this.total += 300;
    };
    RegisterComponent.prototype.smallminus = function () {
        if (this.small > 0) {
            this.small -= 1;
            this.total -= 300;
        }
    };
    RegisterComponent.prototype.madd = function () {
        this.medium += 1;
        this.total += 300;
    };
    RegisterComponent.prototype.mminus = function () {
        if (this.medium > 0) {
            this.medium -= 1;
            this.total -= 300;
        }
    };
    RegisterComponent.prototype.ladd = function () {
        this.large += 1;
        this.total += 350;
    };
    RegisterComponent.prototype.lminus = function () {
        if (this.large > 0) {
            this.large -= 1;
            this.total -= 350;
        }
    };
    RegisterComponent.prototype.xladd = function () {
        this.xlarge += 1;
        this.total += 350;
    };
    RegisterComponent.prototype.xlminus = function () {
        if (this.xlarge > 0) {
            this.xlarge -= 1;
            this.total -= 350;
        }
    };
    RegisterComponent.prototype.xxladd = function () {
        this.xxlarge += 1;
        this.total += 350;
    };
    RegisterComponent.prototype.xxlminus = function () {
        if (this.xxlarge > 0) {
            this.xxlarge -= 1;
            this.total -= 350;
        }
    };
    RegisterComponent.prototype.xxxladd = function () {
        this.xxxlarge += 1;
        this.total += 350;
    };
    RegisterComponent.prototype.xxxlminus = function () {
        if (this.xxxlarge > 0) {
            this.xxxlarge -= 1;
            this.total -= 350;
        }
    };
    RegisterComponent.prototype.click1 = function (add) {
        if (this.isChecked == false) {
            if (this.firstFormGroup.value.occuCtrl == 'Student') {
                this.total += 1000;
            }
            else {
                this.total += 1250;
            }
            for (var x = 0; x < this.additionalParticipants.value.length; x++) {
                if (this.additionalParticipants.value[x].occupation == 'Student') {
                    this.total += 1000;
                }
                else {
                    this.total += 1250;
                }
            }
            this.day1 += add;
            this.isChecked = !this.isChecked;
        }
        else {
            if (this.firstFormGroup.value.occuCtrl == 'Student') {
                this.total -= 1000;
            }
            else {
                this.total -= 1250;
            }
            for (var x = 0; x < this.additionalParticipants.value.length; x++) {
                if (this.additionalParticipants.value[x].occupation == 'Student') {
                    this.total -= 1000;
                }
                else {
                    this.total -= 1250;
                }
            }
            this.day1 = 0;
            this.isChecked = !this.isChecked;
        }
    };
    RegisterComponent.prototype.click2 = function (add) {
        if (this.isChecked2 == false) {
            if (this.firstFormGroup.value.occuCtrl == 'Student') {
                this.total += 1000;
            }
            else {
                this.total += 1250;
            }
            for (var x = 0; x < this.additionalParticipants.value.length; x++) {
                if (this.additionalParticipants.value[x].occupation == 'Student') {
                    this.total += 1000;
                }
                else {
                    this.total += 1250;
                }
            }
            this.day2 += add;
            this.isChecked2 = !this.isChecked2;
        }
        else {
            if (this.firstFormGroup.value.occuCtrl == 'Student') {
                this.total -= 1000;
            }
            else {
                this.total -= 1250;
            }
            for (var x = 0; x < this.additionalParticipants.value.length; x++) {
                if (this.additionalParticipants.value[x].occupation == 'Student') {
                    this.total -= 1000;
                }
                else {
                    this.total -= 1250;
                }
            }
            this.day2 = 0;
            this.isChecked2 = !this.isChecked2;
        }
    };
    RegisterComponent.prototype.click3 = function (add) {
        if (this.isChecked3 == false) {
            this.total += 1500;
            for (var x = 0; x < this.additionalParticipants.value.length; x++) {
                this.total += 1500;
            }
            this.day3 += add;
            this.isChecked3 = !this.isChecked3;
        }
        else {
            this.total -= 1500;
            for (var x = 0; x < this.additionalParticipants.value.length; x++) {
                this.total -= 1500;
            }
            this.day3 = 0;
            this.isChecked3 = !this.isChecked3;
        }
    };
    RegisterComponent.prototype.disap = function () {
        if (this.rcode == "final")
            return false;
        else
            return true;
    };
    RegisterComponent.prototype.resetCart = function () {
        this.noofstud = 0;
        this.noofprof = 0;
        this.day1 = 0;
        this.day2 = 0;
        this.day3 = 0;
        this.total = 0;
        this.isChecked = false;
        this.isChecked2 = false;
        this.isChecked3 = false;
        this.small = 0;
        this.medium = 0;
        this.large = 0;
        this.xlarge = 0;
        this.xxlarge = 0;
        this.xxxlarge = 0;
    };
    RegisterComponent.prototype.onChange = function () {
        this.hide = this.firstFormGroup.value.occuCtrl;
        this.total = this.total - (this.day1 + this.day2 + this.day3);
        this.day1 = 0;
        this.day2 = 0;
        this.day3 = 0;
        this.resetCart();
    };
    RegisterComponent.prototype.add = function (stepper) {
        if (this.firstFormGroup.value.occuCtrl == 'Student') {
            this.noofstud += 1;
        }
        else {
            this.noofprof += 1;
        }
        for (var x = 0; x < this.additionalParticipants.value.length; x++) {
            if (this.additionalParticipants.value[x].occupation == 'Student') {
                this.noofstud += 1;
            }
            else {
                this.noofprof += 1;
            }
        }
        if (this.firstFormGroup.value.firstCtrl != ''
            && this.firstFormGroup.value.lastCtrl != ''
            && this.firstFormGroup.value.asCtrl != ''
            && this.firstFormGroup.value.occuCtrl != ''
            && this.firstFormGroup.value.emailCtrl != '') {
            var conf = 0;
            for (var x = 0; x < this.additionalParticipants.value.length; x++) {
                if (this.additionalParticipants.value[x].first_name == ""
                    || this.additionalParticipants.value[x].last_name == ""
                    || this.additionalParticipants.value[x].occupation == ""
                    || this.additionalParticipants.value[x].agency == ""
                    || this.additionalParticipants.value[x].email == "") {
                    conf = 1;
                    break;
                }
            }
            if (conf == 1) {
                alert("Fill out all the required fields!");
            }
            else
                stepper.next();
        }
        else {
            alert("Fill out all the required fields!");
        }
    };
    RegisterComponent.prototype.register = function (stepper) {
        var _this = this;
        if (this.firstFormGroup.value.firstCtrl != ''
            && this.firstFormGroup.value.lastCtrl != ''
            && this.firstFormGroup.value.asCtrl != ''
            && this.firstFormGroup.value.occuCtrl != ''
            && this.firstFormGroup.value.emailCtrl != '') {
            if (this.day1 == 0 && this.day2 == 0 && this.day3 == 0) {
                alert("You must check at least 1 in the seminar/training days!");
            }
            else {
                if (this.checked == 1) {
                    this.rcode = "load";
                    this.http.get('https://tcon4reg.i-tugue.com/backend/api.php?q=reg&fname=' + this.firstFormGroup.value.firstCtrl
                        + '&mname=' + this.firstFormGroup.value.middleCtrl
                        + '&lname=' + this.firstFormGroup.value.lastCtrl
                        + '&email=' + this.firstFormGroup.value.emailCtrl
                        + '&mobile=' + this.firstFormGroup.value.mobileCtrl
                        + '&rcode=' + this.firstFormGroup.value.codeCtrl
                        + '&agency=' + this.firstFormGroup.value.asCtrl
                        + '&category=' + this.firstFormGroup.value.occuCtrl
                        + '&day1=' + this.day1
                        + '&day2=' + this.day2
                        + '&day3=' + this.day3
                        + '&s=' + this.small
                        + '&m=' + this.medium
                        + '&l=' + this.large
                        + '&xl=' + this.xlarge
                        + '&xxl=' + this.xxlarge
                        + '&xxxl=' + this.xxxlarge)
                        .map(function (response) { return response.json(); })
                        .subscribe(function (res) {
                        if (res.message == "failed") {
                            alert("something went wrong");
                            _this.rcode = "fail";
                        }
                        else {
                            stepper.next();
                            _this.code = res.message;
                            _this.rcode = "final";
                            for (var x = 0; x < _this.additionalParticipants.value.length; x++) {
                                _this.http.get('https://tcon4reg.i-tugue.com/backend/api.php?q=reg&fname=' + _this.additionalParticipants.value[x].first_name
                                    + '&mname=' + _this.additionalParticipants.value[x].middle_name
                                    + '&lname=' + _this.additionalParticipants.value[x].last_name
                                    + '&email=' + _this.additionalParticipants.value[x].email
                                    + '&mobile=' + "-g-"
                                    + '&rcode=' + _this.firstFormGroup.value.codeCtrl
                                    + '&agency=' + _this.additionalParticipants.value[x].agency
                                    + '&category=' + _this.additionalParticipants.value[x].occupation
                                    + '&day1=' + _this.day1
                                    + '&day2=' + _this.day2
                                    + '&day3=' + _this.day3
                                    + '&s=' + "-g-"
                                    + '&m=' + "-g-"
                                    + '&l=' + "-g-"
                                    + '&xl=' + "-g-"
                                    + '&xxl=' + "-g-"
                                    + '&xxxl=' + "-g-")
                                    .map(function (response) { return response.json(); })
                                    .subscribe(function (res) {
                                }, function (Error) {
                                    console.log(Error);
                                    alert("No Internet connection!");
                                    _this.rcode = "fail";
                                });
                            }
                        }
                    }, function (Error) {
                        console.log(Error);
                        alert("No Internet connection!");
                        _this.rcode = "fail";
                    });
                    console.log(this.rcode);
                }
                else {
                    alert("You're a Robot!");
                }
            }
        }
        else {
            alert("Fill out all the required fields!");
        }
    };
    RegisterComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-register',
            template: __webpack_require__("../../../../../src/app/pages/register/register.component.html"),
            styles: [__webpack_require__("../../../../../src/app/pages/register/register.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */]])
    ], RegisterComponent);
    return RegisterComponent;
}());



/***/ }),

/***/ "../../../../../src/app/pages/status/status.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/pages/status/status.component.html":
/***/ (function(module, exports) {

module.exports = "<mat-card>\n  <mat-card-header>\n    <mat-card-title>QR Code</mat-card-title>\n    <mat-card-subtitle>check your status</mat-card-subtitle>\n  </mat-card-header>\n  <mat-card-content>\n\n    <mat-form-field>\n      <input matInput placeholder=\"Reservation Code\" [(ngModel)]=\"code\">\n    </mat-form-field>\n    \n    <div>\n      <qr-code [value]=\"code\" [size]=\"480\"></qr-code>\n    </div>\n  \n  </mat-card-content>\n  <mat-card-actions>\n    <!-- <button mat-button>Ok</button> -->\n  </mat-card-actions>\n</mat-card>"

/***/ }),

/***/ "../../../../../src/app/pages/status/status.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StatusComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var StatusComponent = /** @class */ (function () {
    function StatusComponent() {
        this.code = "";
    }
    StatusComponent.prototype.ngOnInit = function () {
    };
    StatusComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-status',
            template: __webpack_require__("../../../../../src/app/pages/status/status.component.html"),
            styles: [__webpack_require__("../../../../../src/app/pages/status/status.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], StatusComponent);
    return StatusComponent;
}());



/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};


/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_hammerjs__ = __webpack_require__("../../../../hammerjs/hammer.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_hammerjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_hammerjs__);





if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["enableProdMode"])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map
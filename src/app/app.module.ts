import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { MaterialModule } from './material.module';
import { RecaptchaModule } from 'ng-recaptcha';
import { HttpModule } from '@angular/http';

import { QRCodeModule } from 'angular2-qrcode';
import { StatusComponent } from './pages/status/status.component';
import { RegisterComponent } from './pages/register/register.component';
import { SweetAlertService } from 'ng2-sweetalert2';
import { PdfComponent } from './pages/pdf/pdf.component';
import { GlobalvarsService } from './service/globalvars.service';
@NgModule({
  declarations: [
    AppComponent,
    StatusComponent,
    RegisterComponent,
    PdfComponent
  ],
  imports: [
    BrowserModule,MaterialModule,RecaptchaModule.forRoot(),
    AppRoutingModule,BrowserAnimationsModule,
    HttpModule, QRCodeModule
  ],
  providers: [SweetAlertService,GlobalvarsService],
  bootstrap: [AppComponent]
})
export class AppModule { }

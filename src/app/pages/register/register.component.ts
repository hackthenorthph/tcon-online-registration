import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { Headers, RequestOptions} from '@angular/http';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { MatStepper,MatDialog,MatDialogRef } from '@angular/material';
import { PdfComponent } from './../../pages/pdf/pdf.component';
import { SweetAlertService } from 'ng2-sweetalert2';
import Swal from 'sweetalert2';
import { GlobalvarsService } from './../../service/globalvars.service';

import { QRCodeComponent } from 'angular2-qrcode';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  header = new Headers();
  option:any;
  url="https://reg-api.hackthenorth.ph/api/"

  day1Name = "IT Security";
  day2Name = "Debug: Developers Convergence";
  day3Name = "Pitch Please";
  qrcode=''

  day1Stud = 1000;
  day1Prof = 1250;
  day2Stud = 1000;
  day2Prof = 1250;
  day3Stud = 1000;
  day3Prof = 1250;

adds=0;
noofprof=0;
noofstud=0;
  isChecked: boolean = false;
  isChecked3: boolean = false;
  isChecked2: boolean = false;
  isChecked4: boolean = false;
  rcode: string = 'none';
  hide: string = "none";
  isLinear = false;
  firstFormGroup: FormGroup;
  checked;
  total = 0;
  day1 = 0;
  day2 = 0;
  day3 = 0;
  small = 0;
  medium = 0;
  large = 0;
  xlarge = 0;
  xxlarge = 0;
  xxxlarge = 0;
  badge = 0;
  code = "Register First to get your Unique Code!";
  code2 = "The QR code will be used for your attendance, else you will be obliged to encode your name in the booth during the seminar.";
  pass:any = 'wew';
  addParticipantsForm: FormGroup;
  additionalParticipants: any = [];


events;
  constructor(public Globalvars: GlobalvarsService,public dialog: MatDialog,private _formBuilder: FormBuilder, private http: Http) {
   setTimeout(console.log.bind(console, '%cStop!', 'color: red;font-size:75px;font-weight:bold;-webkit-text-stroke: 1px black;'), 0);
    setTimeout(console.log.bind(console, '%cThis is a browser feature intended for developers.', 'color: black;font-size:20px;'), 0);
    
    this.requestToken()
    this.http.get(this.url+'event/list',this.option)
                         .map(response => response.json())
                        .subscribe(res => {
                          this.events = res;
                        }

                        );
  }
  openDialog(): void {
    const dialogRef = this.dialog.open(PdfComponent, {
      width: '100%',
      data: {pass: this.pass}
    });
  }
  resolved(captchaResponse: string) {
    if (captchaResponse != null || captchaResponse != undefined || captchaResponse != '') {
      this.checked = 1;
    } else {
      this.checked = 0;
    }

  }
  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required],
      middleCtrl: [''],
      lastCtrl: ['', Validators.required],
      emailCtrl: [''],
      mobileCtrl: [''],
      codeCtrl: [''],
      asCtrl: [''],
      occuCtrl: ['', Validators.required],
      hometownCtrl: [''],
      additionalParticipants: this._formBuilder.array([this.createParticipant()])
    });    

    this.additionalParticipants = this.firstFormGroup.get('additionalParticipants') as FormArray;
    this.additionalParticipants.removeAt(0);
    
  }

  createParticipant(): FormGroup {
    return this._formBuilder.group({
      first_name: '',
      middle_name: '',
      last_name: '',
      occupation: '',
      mobile: '',
      email: '',
      agency: '',
      hometown: '',
    });
  }

  addParticipant(): void {
    this.additionalParticipants = this.firstFormGroup.get('additionalParticipants') as FormArray;
    this.additionalParticipants.push(this.createParticipant());

  }

  removeParticipant(index) {
    Swal({
      title: 'Are you sure?',
      text: "Remove this participant?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.value) {
        this.additionalParticipants = this.firstFormGroup.get('additionalParticipants') as FormArray;
        this.additionalParticipants.removeAt(index);
        Swal(
          'Removed!',
          'Participant removed!',
          'success'
        )
      }
    })
    
  }

  chect=false
  saveevent=[]
  click1(id,price) {
    if (this.saveevent.includes(id)) {
     for(var i = this.saveevent.length - 1; i >= 0; i--) {
          if(this.saveevent[i] === id) {
             this.saveevent.splice(i, 1);
          }
      }

      if (this.firstFormGroup.value.occuCtrl=='Student') {
        this.total -= (price-500);
      }else{
        this.total -= price;
      }

       for(var x=0;x<this.additionalParticipants.value.length;x++){
        if (this.additionalParticipants.value[x].occupation=='Student') {
          this.total -= (price-500);
        }else{
          this.total -= price;
        }
      }
    }else{
       this.saveevent.push(id)
       if (this.firstFormGroup.value.occuCtrl=='Student') {
        this.total += (price-500);
      }else{
        this.total += price;
      }


      for(var x=0;x<this.additionalParticipants.value.length;x++){
        if (this.additionalParticipants.value[x].occupation=='Student') {
        this.total += (price-500);
      }else{
        this.total += price;
        }
      }
    }


    //   this.day1 += 1;
    //   this.isChecked = !this.isChecked;
    // } else {
      
    //   if (this.firstFormGroup.value.occuCtrl=='Student') {
    //     this.total -= this.day1Stud;
    //   }else{
    //     this.total -= this.day1Prof;
     

    //   this.day1 = 0;
    //   this.isChecked = !this.isChecked;
    // }
  }
  click4() {
    if (this.isChecked4 == false) {
       
      this.isChecked4 = !this.isChecked4;
    } else {
      
      this.isChecked4 = !this.isChecked4;
    }
  }
  disap() {
    if (this.rcode == "final")
      return false;
    else
      return true;
  }

  resetCart() {

    this.chect = false;
    this.saveevent=[]
    this.adds=0;
    this.noofstud = 0;
    this.noofprof = 0;
    this.day1 = 0;
    this.day2 = 0;
    this.day3 = 0;
    this.total = 0;
    this.isChecked = false;
    this.isChecked2 = false;
    this.isChecked3 = false;

    this.small = 0;
    this.medium = 0;
    this.large = 0;
    this.xlarge = 0;
    this.xxlarge = 0;
    this.xxxlarge = 0;
    this.badge = 0;
  }



  add(stepper: MatStepper) {
    this.chect = true;
    if (this.adds==0) {
      this.adds=1;
     if (this.firstFormGroup.value.occuCtrl=='Student') {
        this.noofstud += 1;
      }else{
        this.noofprof += 1;
      }


      for(var x=0;x<this.additionalParticipants.value.length;x++){
        if (this.additionalParticipants.value[x].occupation=='Student') {
          this.noofstud += 1;
        }else{
          this.noofprof += 1;
        }
      }


    if (this.firstFormGroup.value.firstCtrl != ''
      && this.firstFormGroup.value.lastCtrl != ''
      && this.firstFormGroup.value.asCtrl != ''
      && this.firstFormGroup.value.occuCtrl != ''
      && this.firstFormGroup.value.emailCtrl != ''
      && this.firstFormGroup.value.emailCtrl != ''
      && this.firstFormGroup.value.mobileCtrl != ''
      && this.firstFormGroup.value.hometownCtrl != '') {
      
      var conf=0;
      for(var x=0;x<this.additionalParticipants.value.length;x++){

          if (this.additionalParticipants.value[x].first_name == ""
          || this.additionalParticipants.value[x].last_name == ""
          || this.additionalParticipants.value[x].occupation == ""
          || this.additionalParticipants.value[x].mobile == ""
          || this.additionalParticipants.value[x].hometown == ""
          || this.additionalParticipants.value[x].agency == ""
          || this.additionalParticipants.value[x].email == "") {
             conf =1;
             break;
          }
      }
      if (conf==1) {
        Swal("Fill out all the required fields!conf");
        this.adds=0;
        conf=0;
      }else{
        if (this.ValidateEmail(this.firstFormGroup.value.emailCtrl)==true) {
                    var conf2=0;
                      for(var x=0;x<this.additionalParticipants.value.length;x++){
                          if (this.ValidateEmail(this.additionalParticipants.value[x].email)!=true) {
                             conf2 =1;
                             break;
                          }
                      }
                      if (conf2==1) {
                        Swal("Invalid email address");
                        conf2=0;
                      }else{
                        stepper.next();
                      }
        }else{
          Swal("Invalid email address"); 
          this.adds=0;
        }
      }
    } else {
      Swal("Fill out all the required fields!");
       this.adds=0;
    }
  }
  }

  ValidateEmail(mail) 
    {
     if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
      {
        return (true)
      }
        return (false)
    }
  register(stepper) {

    if (this.isChecked4 == true) {
    if (this.firstFormGroup.value.firstCtrl != ''
      && this.firstFormGroup.value.lastCtrl != ''
      && this.firstFormGroup.value.asCtrl != ''
      && this.firstFormGroup.value.occuCtrl != ''
      && this.firstFormGroup.value.emailCtrl != '') {
      if (this.saveevent.length==0) {
        Swal("You must check at least 1 in the seminar/training days!");
      } else {
        if (this.checked == 1) {
          this.rcode = "load";

              this.http.post(this.url+'participant/register', {
                             "first_name": this.firstFormGroup.value.firstCtrl,
                              "middle_name":this.firstFormGroup.value.middleCtrl,
                              "last_name": this.firstFormGroup.value.lastCtrl,
                              "contact_number": this.firstFormGroup.value.mobileCtrl,
                              "email": this.firstFormGroup.value.emailCtrl,
                              "hometown": this.firstFormGroup.value.hometownCtrl,
                              "occupation": this.firstFormGroup.value.occuCtrl,
                              "agency": this.firstFormGroup.value.asCtrl
                         },this.option)
                         .map(response => response.json())
                        .subscribe(res => {
                          stepper.next();

                          this.qrcode="TCON"+res.participant.id+"2019";

                          var storedcode=res.participant.id
                          this.code = this.firstFormGroup.value.firstCtrl+this.firstFormGroup.value.lastCtrl;
                          this.rcode = "final";
                          for (var i = this.saveevent.length - 1; i >= 0; i--) {
                             this.http.post(this.url+'event/register', {
                                   "event_id": this.saveevent[i],
                                    "participant_id":res.participant.id
                               },this.option)
                               .map(response => response.json())
                              .subscribe(res => {});
                          }
                
                for(var x=0;x<this.additionalParticipants.value.length;x++){
                      this.http.post(this.url+'participant/register?group_id='+storedcode, {
                             "first_name": this.additionalParticipants.value[x].first_name,
                              "middle_name":this.additionalParticipants.value[x].middle_name,
                              "last_name": this.additionalParticipants.value[x].last_name,
                              "contact_number": this.additionalParticipants.value[x].mobile,
                              "email": this.additionalParticipants.value[x].email,
                              "hometown": this.additionalParticipants.value[x].hometown,
                              "occupation": this.additionalParticipants.value[x].occupation,
                              "agency": this.additionalParticipants.value[x].agency
                         },this.option)
                         .map(response => response.json())
                        .subscribe(data => {
                          for (var i = this.saveevent.length - 1; i >= 0; i--) {
                             this.http.post(this.url+'event/register', {
                                   "event_id": this.saveevent[i],
                                    "participant_id":res.participant.id
                               },this.option)
                               .map(response => response.json())
                              .subscribe(res => {console.log(res)});
                          }
                       },err=>{
                          console.log(Error);
              Swal("No Internet connection/Server down!");  
                          this.rcode = "fail";
                       });
                }
                Swal(
                  'Registration Complete!',
                  'You may now proceed with paying your Registration Fee. Please print a copy of the QR code as a proof of your registration. Thank you!',
                  'success'
                );
                    this.Globalvars.firstFormGroup=this.firstFormGroup;
                    this.Globalvars.additionalParticipants=this.additionalParticipants;
                    this.Globalvars.day1=this.day1;
                    this.Globalvars.day2=this.day2;
                    this.Globalvars.day3=this.day3;
                    this.Globalvars.noofprof=this.noofprof;
                    this.Globalvars.noofstud=this.noofstud;
                    this.Globalvars.small=this.small;
                    this.Globalvars.medium=this.medium;
                    this.Globalvars.large=this.large;
                    this.Globalvars.xlarge=this.xlarge;
                    this.Globalvars.xxlarge=this.xxlarge;
                    this.Globalvars.xxxlarge=this.xxxlarge;
                    this.Globalvars.total=this.total;
                    this.Globalvars.rcode=this.rcode;
                    this.Globalvars.code=this.code;
                    this.Globalvars.code2=this.code2;
                    this.Globalvars.badge=this.badge;
            }, Error => {
              console.log(Error);
              Swal("No Internet connection/Server down!");
              this.rcode = "fail";
            });

        } else {
          Swal("You're a Robot!");
        }
      }
    }
    else {
      Swal("Fill out all the required fields!");
    }
  }else
  {
    Swal("Please check the confirmation to register");
  }
  }

  requestToken(){
    this.header.append("Content-Type", "application/json");
    this.header.append("Accept","application/json");
    this.option = new RequestOptions({ headers: this.header });
  }


toTimestamp(strDate){
   var datum = Date.parse(strDate);
   return datum/1000;
}
   process(time){
     time = this.toTimestamp(time);
            var date1 = new Date(time*1000);
            var date2 = new Date();
              return date1.toDateString()
          }
}

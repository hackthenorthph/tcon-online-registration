import { Component, OnInit } from '@angular/core';
import { QRCodeComponent } from 'angular2-qrcode';

@Component({
  selector: 'app-status',
  templateUrl: './status.component.html',
  styleUrls: ['./status.component.css']
})
export class StatusComponent implements OnInit {

  code: string = "";

  constructor() { }

  ngOnInit() {
  }

}

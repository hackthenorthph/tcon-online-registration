import { Component, OnInit } from '@angular/core';
import { GlobalvarsService } from './../../service/globalvars.service';
import { QRCodeComponent } from 'angular2-qrcode';

@Component({
  selector: 'app-pdf',
  templateUrl: './pdf.component.html',
  styleUrls: ['./pdf.component.css']
})
export class PdfComponent implements OnInit {
firstFormGroup:any;
additionalParticipants:any;
day1:any;
day2:any;
day3:any;
noofprof:any;
noofstud:any;
small:any;
medium:any;
large:any;
xlarge:any;
xxlarge:any;
xxxlarge:any;
total:any;
rcode:any;
code:any;
code2:any;
badge:any;
  constructor(public Globalvars: GlobalvarsService) {
                    this.firstFormGroup=this.Globalvars.firstFormGroup;
                    this.additionalParticipants=this.Globalvars.additionalParticipants;
                    this.day1=this.Globalvars.day1;
                    this.day2=this.Globalvars.day2;
                    this.day3=this.Globalvars.day3;
                    this.noofprof=this.Globalvars.noofprof;
                    this.noofstud=this.Globalvars.noofstud;
                    this.small=this.Globalvars.small;
                    this.medium=this.Globalvars.medium;
                    this.large=this.Globalvars.large;
                    this.xlarge=this.Globalvars.xlarge;
                    this.xxlarge=this.Globalvars.xxlarge;
                    this.xxxlarge=this.Globalvars.xxxlarge;
                    this.total=this.Globalvars.total;
                    this.rcode=this.Globalvars.rcode;
                    this.code=this.Globalvars.code;
                    this.code2=this.Globalvars.code2;
                    this.badge=this.Globalvars.badge;
   }
print(): void {
    let printContents, popupWin;
    printContents = document.getElementById('print-section').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Print tab</title>
          <style>
          //........Customized style.......
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
}
  ngOnInit() {
  }

}
